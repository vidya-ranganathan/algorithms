#include "listHelper.h"

void recursiveReverse(struct linkList** listhead)
{
    struct linkList* curr;
    struct linkList* next;
 
    /* empty list */
    if (*listhead == NULL)
       return;   
 
    /* suppose curr = {1, 2, 3}, next = {2, 3} */
    curr = *listhead;
    next  = curr->next;
 
    /* List has only one node */
    if (next == NULL)
       return;   
 
    /* put the curr element on the end of the list */
    recursiveReverse(&next);
    curr->next->next  = curr;  
 
    curr->next  = NULL;          
 
    /* fix the head pointer */
    *listhead = next;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert linkList in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);

	displayList(list1);

	recursiveReverse(&list1);

	displayList(list1);
}