#include "listHelper.h"

void swapAlt(struct linkList **head) {
	struct linkList *curr , *temp , *next;
	curr = *head;
	*head = curr->next;
	
	while(curr != NULL && curr->next != NULL) {
		next = curr->next;
		curr->next = curr->next->next;
		next->next = curr;
		temp = curr->next;
		if(curr->next && curr->next->next)
			curr->next = curr->next->next;
		curr = temp;
	}
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 98, 8);

	displayList(list1);

	swapAlt(&list1);

	displayList(list1);
}