#include "listHelper.h"

int findLoopByFloyds_method(struct linkList *list1) {
	struct linkList *head, *hare, *tortoise;
	head = hare = tortoise = list1;
	int loopExists;

	while(hare && tortoise) {
		hare = hare->next;
		if(hare == tortoise) {
			loopExists = 1;
			break;
		}

		if(hare==NULL) {
			loopExists = 0;
		}

		hare = hare->next;	
		if(hare == tortoise) {
			loopExists = 1;
			break;
		}

		tortoise = tortoise->next;	
	}

	printf("%d..%d\n", tortoise->data, hare->data);

	/*
	 * if loop exists , keep tortoise at head and 
	 * hare hare at tracker.
	 * now , loop hare-next until we dont hit tracker
	 */

	tortoise = head;
	struct linkList *tracker = hare;
	if(loopExists) {
		while(1)
		{
			do {
				hare = hare->next;
				if(hare == tortoise) {
					printf("%d..%d\n", tortoise->data, hare->data);
					return tortoise->data;
				}
			}while(hare != tracker);
			tortoise = tortoise->next;
		}
	}

	return 0;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 98, 8);

	/* create Loop at node = 61 */
	list1->next->next->next->next->next->next->next->next = list1->next->next->next->next;

	int merge;
	if( merge = findLoopByFloyds_method(list1))
		printf("the loop starts at node=%d\n", merge);
	else
		printf(" no loop\n");

}