#include "listHelper.h"

void moveLast2First(struct linkList **headNode)
{
	struct linkList *curr = *headNode;
	struct linkList *lastButOne, *last;

	while(curr->next != NULL)  {
		if(curr->next->next == NULL)
		{
			lastButOne = curr;
			curr = curr->next;
			break;
		}
		curr = curr->next;
	}
	last = curr;

	lastButOne->next = NULL;
	last->next = *headNode;
	*headNode = last;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 98, 8);

	displayList(list1);
	moveLast2First(&list1);
	displayList(list1);

}