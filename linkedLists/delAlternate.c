#include "listHelper.h"

void delAlt(struct linkList **headNode)
{
	struct linkList *curr = *headNode;
	struct linkList *delNode;

	while(curr != NULL && curr->next != NULL) {
		delNode = curr->next;
		curr->next = curr->next->next;
		curr = curr->next;
		free(delNode);	
	}
	
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 98, 8);
        InsertLinkedList(&list1, 99, 9);
        InsertLinkedList(&list1, 111, 10);
        InsertLinkedList(&list1, 112, 11);

	displayList(list1);
	delAlt(&list1);
	displayList(list1);
}