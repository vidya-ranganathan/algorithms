#include "listHelper.h"


void deleteNode(struct linkList **list, struct linkList *del){

	struct linkList *locList = *list;

	if(*list == NULL || del == NULL)
		return;

	while(locList != NULL) {
		if(locList->next == del) {
			locList->next = locList->next->next;
			free(del);
			break;	
		}
		locList = locList->next;
	}
	return;
}

main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 12, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 15, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 10, 3);
        InsertLinkedList(&list1, 11, 4);
        InsertLinkedList(&list1, 5, 5);
        InsertLinkedList(&list1, 6, 6);
        InsertLinkedList(&list1, 2, 7);
        InsertLinkedList(&list1, 3, 8);

	displayList(list1);
	deleteNode(&list1, list1->next->next);
	displayList(list1);

}