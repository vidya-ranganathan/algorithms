#include "listHelper.h"

int nthNodeFromEnd(struct linkList *list, int nEnd) {
	struct linkList *ptr1, *ptr2;
	ptr1 = ptr2 = list;

	/* walk n nodes from start on ptr1, 
	 * then walk both nodes such that 
	 * ptr1 reaches NULL , we would have
	 * ptr2->data will be same.
	 */
	while(nEnd != 0) {
		ptr1 = ptr1->next;
		nEnd --;
	}
	
	while(ptr1 != NULL) {
		ptr1= ptr1->next;
		ptr2= ptr2->next;
	}
	return (ptr2->data);
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 91, 8);
        InsertLinkedList(&list1, 44, 9);
        InsertLinkedList(&list1, 3, 10);
        InsertLinkedList(&list1, 7, 11);

	int nEnd = 4;

	printf ( "%dth node from End is %d\n", nEnd, nthNodeFromEnd(list1, nEnd));
        displayList(list1);

}