#include "listHelper.h"

int reverse(struct linkList **list, struct linkList **rev){
	struct linkList *loclist = NULL, *locrev = NULL, *lochead = NULL;
	loclist = *list;
	
	/* placing nodes into head of rev list itself reverses the current
	 * list.
	 */

	while(loclist != NULL)
	{
		locrev = (struct linkList *)malloc(sizeof(struct linkLink *));
		if(locrev == NULL)
		{
			printf("ENOMEM");
			return 1;
		}
		locrev->data = loclist->data;
		locrev->next = lochead;
		lochead = locrev;
		loclist = loclist->next;
		
	}
	*rev = locrev;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL, *rev;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 91, 8);
        InsertLinkedList(&list1, 44, 9);
        InsertLinkedList(&list1, 3, 10);
        InsertLinkedList(&list1, 7, 11);


        displayList(list1);
	reverse(&list1, &rev);
	displayList(rev);

}