#include "listHelper.h"

int nthNode(struct linkList *list, int nNode) {

	/* assume that there are atleast n nodes 
	 * in the list. Error handling not done 
	 * here..
	 */

	while(nNode)
	{
		nNode--;
		if(nNode)
		list = list->next;
	}
	return list->data;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 91, 8);
        InsertLinkedList(&list1, 44, 9);
        InsertLinkedList(&list1, 3, 10);
        InsertLinkedList(&list1, 7, 11);

	int nNode= 4;

	printf ( "%dth node from End is %d\n", nNode, nthNode(list1, nNode));
        displayList(list1);

}