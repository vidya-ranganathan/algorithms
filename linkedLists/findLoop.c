#include "listHelper.h"

int findLoopByFloyds_method(struct linkList *list1) {
	struct linkList *head, *hare, *tortoise;
	head = hare = tortoise = list1;

	if(hare->next == NULL)
		return 0;		/* no loop , only one node in the list  */

	while(hare && tortoise) {
		hare = hare->next;
		if(hare == tortoise)
			return 1;	/* hare runs faster and meets tortoise, there is a loop */
		if(hare==NULL)
			return 0;	/* No loop */
		hare = hare->next;	/* hare runs one more step faster */
		if(hare == tortoise)
                        return 1;       /* hare runs faster and meets tortoise, there is a loop */

		tortoise = tortoise->next;	/* tortoise slowly moves */
	}
	return 0;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 41, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 61, 5);
        InsertLinkedList(&list1, 72, 6);
        InsertLinkedList(&list1, 87, 7);
        InsertLinkedList(&list1, 98, 8);

	/* create Loop at node = 61 */
	list1->next->next->next->next->next->next->next->next = list1->next->next->next->next;

	if(findLoopByFloyds_method(list1))
		printf("linked list has loop\n");
	else
		printf(" no loop\n");

}