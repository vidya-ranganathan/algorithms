#include "listHelper.h"

int isPalindrome(struct linkList  **head, struct  linkList *tail)
{
 	static int palindrome;

   	if (tail == NULL)
      		return 1;
 
	palindrome = isPalindrome(head, tail->next);
		if(palindrome == 0)
			return 0;

	if((*head)->data == tail->data)
		palindrome = 1;
	else
		palindrome = 0;

   	*head = (*head)->next; /* save next pointer */
 
   	return palindrome;
}

void
main() {

        int dat;
        struct linkList *list1 = NULL;
        struct linkList *copy = NULL;

        InsertLinkedList(&list1, 21, 1);         /*Insert node in Beginning */
        InsertLinkedList(&list1, 31, 2);         /*Insert at position 2 */
        InsertLinkedList(&list1, 61, 3);
        InsertLinkedList(&list1, 51, 4);
        InsertLinkedList(&list1, 71, 5);
        InsertLinkedList(&list1, 51, 6);
        InsertLinkedList(&list1, 61, 7);
        InsertLinkedList(&list1, 31, 8);
        InsertLinkedList(&list1, 21, 9);

        displayList(list1);

	copy = list1;

	if(isPalindrome(&list1, list1))
		printf("list is palindrome\n");
	else
		printf(" list is NOT palindrome\n");

        displayList(copy);

}